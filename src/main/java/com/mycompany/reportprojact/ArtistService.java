/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.reportprojact;

import java.util.List;

/**
 *
 * @author Acer
 */
public class ArtistService {
    public List<ArtistReport> getTopTenArtistByToTalPrice(){
        ArtistDao artistDao = new ArtistDao();
        return artistDao.getArtistByToTalPrice(10);
    }
    public List<ArtistReport> getTopTenArtistByToTalPrice(String begin, String end){
        ArtistDao artistDao = new ArtistDao();
        return artistDao.getArtistByToTalPrice(begin,end,10);
    }
}
