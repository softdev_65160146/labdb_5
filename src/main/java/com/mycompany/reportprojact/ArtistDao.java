/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.reportprojact;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Acer
 */
public class ArtistDao implements Dao<Artist> {

    @Override
    public Artist get(int id) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Artist> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Artist save(Artist obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public Artist update(Artist obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public int delete(Artist obj) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    @Override
    public List<Artist> getAll(String where, String order) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public List<ArtistReport> getArtistByToTalPrice(int limit) {
        ArrayList<ArtistReport> list = new ArrayList();
        String sql = """
                     SELECT ART.*,SUM(INI.Quantity) AS TotalQuantity, SUM(INI.UnitPrice* INI.Quantity) AS TotalPrice
                     FROM ARTISTS ART
                     INNER JOIN ALBUMS ALB ON ALB.ArtistId = ART.ArtistId
                     INNER JOIN TRACKS TRA ON TRA.AlbumId=ALB.AlbumId
                     INNER JOIN INVOICE_ITEMS INI ON INI.TrackId=TRA.TrackId
                     INNER JOIN INVOICES INV ON INV.InvoiceId=INI.InvoiceId
                     GROUP BY ART.ArtistId
                     ORDER BY TotalQuantity DESC
                     LIMIT ?
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ArtistReport obj = ArtistReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }

    public List<ArtistReport> getArtistByToTalPrice(String begin, String end, int limit) {
        ArrayList<ArtistReport> list = new ArrayList();
        String sql = """
                     SELECT ART.*,SUM(INI.Quantity) AS TotalQuantity, SUM(INI.UnitPrice* INI.Quantity) AS TotalPrice
                     FROM ARTISTS ART
                     INNER JOIN ALBUMS ALB ON ALB.ArtistId = ART.ArtistId
                     INNER JOIN TRACKS TRA ON TRA.AlbumId=ALB.AlbumId
                     INNER JOIN INVOICE_ITEMS INI ON INI.TrackId=TRA.TrackId
                     INNER JOIN INVOICES INV ON INV.InvoiceId=INI.InvoiceId
                     AND INV.InvoiceDate BETWEEN ? AND ?
                     GROUP BY ART.ArtistId
                     ORDER BY TotalQuantity DESC
                     LIMIT ?
                     """;
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(3, begin);
            stmt.setString(3, end);
            stmt.setInt(3, limit);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                ArtistReport obj = ArtistReport.fromRS(rs);
                list.add(obj);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
        }
        return list;
    }
}
